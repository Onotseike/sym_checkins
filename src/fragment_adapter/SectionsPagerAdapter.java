package fragment_adapter;

import com.symptom.symptom_checkins.DoctorActivity;
import com.symptom.symptom_checkins.PatientActivity;
import com.symptom.symptom_checkins.R;
import com.symptom.symptom_checkins.R.id;
import com.symptom.symptom_checkins.R.layout;
import com.symptom.symptom_checkins.R.menu;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.view.Menu;
import android.view.MenuItem;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
	
	public SectionsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class
		// below).
		switch (position) {
        case 0:
            // Top Rated fragment activity
            return new PatientActivity();
        case 1:
            // Games fragment activity
            return new DoctorActivity();
                }
 
        return null;
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 2;
	}
	
	
}