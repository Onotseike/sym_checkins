package com.symptom.symptom_checkins;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public  class ListViewAdapter extends BaseAdapter  {
	Context alert_context;
	LayoutInflater alert_inflater;
	private List<Alerts> alerts_list = null;
	private ArrayList<Alerts> alerts_array;
	private SparseBooleanArray selected_item;
	
	public ListViewAdapter(Context context, List<Alerts> alerts_list){
		alert_context = context;
		this.alerts_list = alerts_list;
		alert_inflater = LayoutInflater.from(alert_context);
		this.alerts_array = new ArrayList<Alerts>();
		this.alerts_array.addAll(alerts_list);
	}
	
	//private TextView alerting;
	public class ViewHolder {
		//private 
		TextView alerting;

	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alerts_list.size();	
		}

	@Override
	public Alerts getItem(int position) {
		// TODO Auto-generated method stub
		return alerts_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder alert_holder;
		if(view == null)
		{
			alert_holder = new ViewHolder();
			view = alert_inflater.inflate(R.layout.listview_alert, null);
			alert_holder.alerting = (TextView) view.findViewById(R.id.alerta);
			view.setTag(alert_holder);
			
		}
		else
		{
			alert_holder = (ViewHolder)view.getTag();
		}
		
		alert_holder.alerting.setText(alerts_list.get(position).getAlerts());
		
		view.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0){
				Intent intent = new Intent(alert_context, Single_alertView.class);
				intent.putExtra("ALERT! : ", (alerts_list.get(position).getAlerts()));
				alert_context.startActivity(intent);
			}
		});
		return view;
	}
	
	
	public void filter(String charText) {
		// TODO Auto-generated method stub
		charText = charText.toLowerCase(Locale.getDefault());
		alerts_list.clear();
		if (charText.length() == 0){
			alerts_list.addAll(alerts_array);
		}
		else{
			for (Alerts alert_arr : alerts_array)
			{
				if (alert_arr.getAlerts().toLowerCase(Locale.getDefault()).contains(charText))
				{
					alerts_list.add(alert_arr);
				}
			}
		}
		notifyDataSetChanged();
	}

	public void removeSelection() {
		// TODO Auto-generated method stub
		selected_item = new SparseBooleanArray();
		notifyDataSetChanged();
		
	}

	public void toggleSelection(int position) {
		// TODO Auto-generated method stub
		selectView(position, !selected_item.get(position));
		
	}

	public void selectView(int position, boolean b) {
		// TODO Auto-generated method stub
		if (b)
			selected_item.put(position,b);
		else
			selected_item.delete(position);
		notifyDataSetChanged();
		
		
	}

	public SparseBooleanArray getSelectedIds()
	{
		return selected_item;
	}
		
	

}

	


	
