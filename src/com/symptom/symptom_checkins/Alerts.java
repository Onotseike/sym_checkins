package com.symptom.symptom_checkins;

public class Alerts {
	
	private String alert_notif;
	private String cancer;
	
	public Alerts(String alert_notif,String cancer){
		this.alert_notif = alert_notif;
		this.cancer = cancer;
	}
	
	public String getAlerts(){
		return this.alert_notif;
	}

}
