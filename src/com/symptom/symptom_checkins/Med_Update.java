package com.symptom.symptom_checkins;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Med_Update extends Activity {
	
	TextView text_alerts;
	Button save;
	String str_alerts;
	String str_cancers;
	String str_check;
	TextView text_cancers;
	TextView text_recent;
	String[] cancers = new String[] {"Throat Cancer", "Lung Cancer","Colon Cancer","Cervical Cancer","Brain Cancer","Skin Cancer"};
	String[] recent_check = new String[] {"Check-In 28/11/2014 12:09pm"};
	
	

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.med_update);
		
		Intent intent = getIntent();
		str_alerts = intent.getStringExtra("ALERT! : "); //Get results for alerts
		str_cancers = cancers[0];// intent.getStringExtra("Cancer...");
		str_check = recent_check[0];
		text_alerts = (TextView) findViewById(R.id.alerts);
		text_cancers = (TextView) findViewById(R.id.textView2);
		text_recent = (TextView) findViewById(R.id.textView4);
		save = (Button) findViewById(R.id.button1);
		text_alerts.setText(str_alerts);
		text_cancers.setText(str_cancers);
	
	save.setOnClickListener(new View.OnClickListener() {
		
		public void onClick(View v) {
			
			final Intent update = new Intent(getApplicationContext(),Single_alertView.class); 
						//new Intent(PatientActivity.this,Patient_Dashboard.class);
				startActivity(update);
			
		}
	});
	}
}

