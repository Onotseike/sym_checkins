package com.symptom.symptom_checkins;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Patient_Dashboard extends Activity {
	
	private static final EditText EditText = null;
	private ArrayList<String> answers = new ArrayList<String>();
	private Button camera ;
	private Button submit ;
	private Camera pic_checkin;
	private int pic_id = 2500;
	
	
	RadioButton well_control;
	RadioButton moderate;
	 RadioButton severe ;
	
	
	 RadioButton yes2a ;
	 RadioButton no2a;
	
	 RadioButton yes2b ;
	 RadioButton no2b;
	
	 RadioButton yes2c ;
	 RadioButton no2c;
	
	
	 RadioButton no;
	 RadioButton some;
	 RadioButton cant_eat;
	 RadioButton cant_drink;
	 RadioButton cant_eat_drink;
	
	TextView pain_sore;
	TextView pain_meda;
	TextView pain_medb;
	TextView pain_medc;
	
	TextView pain_eat_drink;
	
	EditText yes_xt;
	EditText yes_xd;
	EditText yes_yt;
	EditText yes_yd;
	EditText yes_zt;
	EditText yes_zd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient__dashboard);
		camera = (Button) findViewById(R.id.button1);
		submit = (Button) findViewById(R.id.button2);
		
		pain_sore = (TextView)findViewById(R.id.textView3);
		pain_meda = (TextView)findViewById(R.id.textView5);
		pain_medb = (TextView)findViewById(R.id.textView6);
		pain_medc = (TextView)findViewById(R.id.textView7);
		pain_eat_drink = (TextView)findViewById(R.id.textView8);
		
		
		yes_xt = (EditText);findViewById(R.id.editText1);
		yes_xd = (EditText);findViewById(R.id.editText2);
		yes_yt = (EditText);findViewById(R.id.editText3);
		yes_yd = (EditText);findViewById(R.id.editText4);
		yes_zt = (EditText);findViewById(R.id.editText5);
		yes_zd = (EditText);findViewById(R.id.editText6);
		
		submit.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						if (answers.size() == 0)
						{
							Toast.makeText(getApplicationContext(), "Please Fill the Checkin Form", Toast.LENGTH_SHORT).show();
						}
						else
						{
							Toast.makeText(getApplicationContext(), "Submission Successful.", Toast.LENGTH_SHORT).show();
							submit.setText(answers.get(1).toString());
						}
						
						//print(answers);
						
					}
				});
	
		camera.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent cam = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cam,pic_id);
				//print(answers);
				
			}
		});

	final OnClickListener painSoreThroat = new OnClickListener()
	{
		@Override
		public void onClick(View v){
			RadioButton radio_button = (RadioButton)v;
			pain_sore.setText(radio_button.getText()+ "CHOSEN");
			answers.add(radio_button.getText().toString());
			
			
		}
	};
	///////////////Medications a b c 
	final OnClickListener painMedications1 = new OnClickListener()
	{
		@Override
		public void onClick(View v){
			RadioButton radio_button = (RadioButton)v;
			
			pain_meda.setText(radio_button.getText()+ "CHOSEN");
			
			answers.add(radio_button.getText().toString());
			
		}
	};
	
	final OnClickListener painMedications2 = new OnClickListener()
	{
		@Override
		public void onClick(View v){
			RadioButton radio_button = (RadioButton)v;
			pain_medb.setText(radio_button.getText()+ " CHOSEN");
			
			answers.add(radio_button.getText().toString());
			
		}
	};
	
	final OnClickListener painMedications3 = new OnClickListener()
	{
		@Override
		public void onClick(View v){
			RadioButton radio_button = (RadioButton)v;
			pain_medc.setText(radio_button.getText()+ " CHOSEN");
			
			answers.add(radio_button.getText().toString());
			
		}
	};
		
	/////////////////Eat or Drink
	final OnClickListener cantEatOrDrink = new OnClickListener()
	{
		@Override
		public void onClick(View v){
			RadioButton radio_button = (RadioButton)v;
			pain_eat_drink.setText(radio_button.getText()+ "  CHOSEN");
			answers.add(radio_button.getText().toString());
			
		}
	};
	
	
////Radio Buttons
	///Pain and Sore Throat
	well_control = (RadioButton) findViewById(R.id.radioButton1);
	moderate = (RadioButton) findViewById(R.id.radioButton2);
	severe = (RadioButton) findViewById(R.id.radioButton3);
	
	well_control.setOnClickListener(painSoreThroat);
	moderate.setOnClickListener(painSoreThroat);
	severe.setOnClickListener(painSoreThroat);
///////////////////////////////////////////////////////////////
	
	//Medications
	//1
	yes2c = (RadioButton) findViewById(R.id.radioButton4);
	no2c = (RadioButton) findViewById(R.id.radioButton5);
	yes2c.setOnClickListener(painMedications1);
	no2c.setOnClickListener(painMedications1);
	
	yes2a = (RadioButton) findViewById(R.id.radioButton6);
	no2a = (RadioButton) findViewById(R.id.radioButton7);
	yes2a.setOnClickListener(painMedications2);
	no2a.setOnClickListener(painMedications2);
	
	
	yes2b = (RadioButton) findViewById(R.id.radioButton8);
	no2b = (RadioButton) findViewById(R.id.radioButton9);
	yes2b.setOnClickListener(painMedications3);
	no2b.setOnClickListener(painMedications3);

	no = (RadioButton) findViewById(R.id.radioButton10);
	some = (RadioButton) findViewById(R.id.radioButton11);
	cant_eat = (RadioButton) findViewById(R.id.radioButton12);
	cant_drink = (RadioButton) findViewById(R.id.radioButton13);
	cant_eat_drink = (RadioButton) findViewById(R.id.radioButton14);
	no.setOnClickListener(cantEatOrDrink);
	some.setOnClickListener(cantEatOrDrink);
	cant_eat.setOnClickListener(cantEatOrDrink);
	cant_drink.setOnClickListener(cantEatOrDrink);
	cant_eat_drink.setOnClickListener(cantEatOrDrink);

	/////////////////////////////////////////////////////////////
	
	}
	
	protected void onActivityResult(int requestcode, int resultCode, Intent data)
	{
		if (requestcode == pic_id)
		{
			Bitmap img = (Bitmap) data.getExtras().get("data");
			ImageView imgView = (ImageView)findViewById(R.id.imageView1);
			imgView.setImageBitmap(img);
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId())
		{
		
		case R.id.action_settings:
			Toast.makeText(getApplicationContext(), "Settings Page", Toast.LENGTH_LONG).show();
			return true;
	
		
		case R.id.set_checkin:
			Intent checkin = new Intent(this.getApplicationContext(), Schedule_CheckIns.class);
			Toast.makeText(getApplicationContext(), "Schedulling  Page", Toast.LENGTH_LONG).show();
			startActivity(checkin);
			return true;
		
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
	
	private boolean checkSubmit()
	{
		return new Random().nextBoolean();
	}
	
	
}
