package com.symptom.symptom_checkins;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class Timepicker extends Activity {
	TimePicker set_time;
	Button set ;
	TextView info;
	final static int rqs = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timepicker);
		set_time = (TimePicker) findViewById(R.id.timePicker1);
		info = (TextView) findViewById(R.id.textView1);
		
		Calendar calender = Calendar.getInstance();
		
		set_time.setCurrentHour(calender.get(Calendar.HOUR_OF_DAY));
		set_time.setCurrentMinute(calender.get(Calendar.MINUTE));
		
		set = (Button) findViewById(R.id.button1);
		
		set.setOnClickListener (new OnClickListener(){
			

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar current = Calendar.getInstance();
				
				Calendar cal = Calendar.getInstance();
				
				cal.set(set_time.getCurrentHour(),set_time.getCurrentMinute(),00);
				if (cal.compareTo(current) <= 0)
				{
					Toast.makeText(getApplicationContext(), "Invalid date Time", Toast.LENGTH_LONG).show();
					setAlarm(cal);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Alarm Set Sucessfully", Toast.LENGTH_LONG).show();
					setAlarm(cal);
				}
				
				
			}

			private void setAlarm(Calendar cal) {
				// TODO Auto-generated method stub
				info.setText("\n\n***\n" + "Alarm is Set @" + cal.getTime() + "\n" + "***\n");
				
				Intent intent = new Intent(getBaseContext(),CheckInReciever.class);
				PendingIntent pends = PendingIntent.getBroadcast(getBaseContext(), rqs, intent, 0);
				AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pends);
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.timepicker, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
