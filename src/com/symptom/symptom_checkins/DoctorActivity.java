package com.symptom.symptom_checkins;

import java.util.Random;

import com.symptom.symptom_checkins.Doctor_Dashboard;
import com.symptom.symptom_checkins.R;
import com.symptom.symptom_checkins.Schedule_CheckIns;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DoctorActivity extends Fragment {
	
	private EditText firstname ;
	private EditText med_id ;
	
	private Button signup;
	private EditText middlename ;
	private EditText lastname ;
	
	private EditText shared_key ;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_doctor, container,
				false);
		final Button signupdoc = (Button)rootView.findViewById(R.id.button2);
		signupdoc.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				if(checkSignup(firstname.getText(),lastname.getText(),med_id.getText(),shared_key.getText()))
				{
					Toast.makeText(getActivity().getApplicationContext(), "Signing Up...", Toast.LENGTH_SHORT).show();
					final Intent doc_dashboard = new Intent(getActivity().getApplicationContext(),Doctor_Dashboard.class); 
					//new Intent(PatientActivity.this,Patient_Dashboard.class);
					startActivity(doc_dashboard);
				}
				
				else
				{
					Toast.makeText(getActivity(), "Try Again...", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		final Button login_doc = (Button) rootView.findViewById(R.id.button1);
		login_doc.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				if(checkLogin(firstname.getText(), med_id.getText(),shared_key.getText())){
					Toast.makeText(getActivity().getApplicationContext(), "Login Successfull.....", Toast.LENGTH_SHORT).show();
					final Intent doc_dashboard = new Intent(getActivity().getApplicationContext(),Doctor_Dashboard.class); 
					//new Intent(PatientActivity.this,Patient_Dashboard.class);
					startActivity(doc_dashboard);
				}
				else
				{
					firstname.setText("Try Again");
					shared_key.setText("Try again");
					Toast.makeText(getActivity().getApplicationContext(), "Login Failed.....", Toast.LENGTH_SHORT).show();
					/*final Intent doc_dashboard = new Intent(getActivity().getApplicationContext(),Doctor_Dashboard.class); 
					//new Intent(PatientActivity.this,Patient_Dashboard.class);
					startActivity(doc_dashboard);*/
				}
			}
		});
		
		return rootView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		firstname = (EditText)getActivity().findViewById(R.id.editText1);
		lastname = (EditText)getActivity().findViewById(R.id.editText2);
		middlename = (EditText)getActivity().findViewById(R.id.editText3);
		shared_key = (EditText)getActivity().findViewById(R.id.editText5);
		signup = (Button)getActivity().findViewById(R.id.button2);
		med_id = (EditText)getActivity().findViewById(R.id.editText6); 
		
	}
	
	private boolean checkLogin(Editable username,Editable userMed_Num,Editable userShared_Key){
		return new Random().nextBoolean();
		
	}
	
	private boolean checkSignup(Editable userfirst,Editable userlast,Editable userMed_Num,Editable userShared_Key){
		return new Random().nextBoolean();
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId())
		{
		
		case R.id.action_settings:
			Toast.makeText(getActivity().getApplicationContext(), "Settings Page", Toast.LENGTH_LONG).show();
			return true;
	
		
		case R.id.set_checkin:
			Intent checkin = new Intent(getActivity().getApplicationContext(), Schedule_CheckIns.class);
			Toast.makeText(getActivity().getApplicationContext(), "Schedulling  Page", Toast.LENGTH_LONG).show();
			startActivity(checkin);
			return true;
		
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
}
