package com.symptom.symptom_checkins;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class Schedule_CheckIns extends Activity {
	
	private PendingIntent pending_intent;
	private int hr;
	private int min;
	//private int sec;
	
	/*static final int date_dialog_id = 0;
	private  TimePickerDialog.OnTimeSetListener time_listener = new TimePickerDialog.OnTimeSetListener(){
		public void onTimeSet(TimePicker view , int hourOfDay, int minute){
			hr = hourOfDay;
			min = minute;
			updateDisplay();
		}

		private void updateDisplay() {
			// TODO Auto-generated method stub
			
		}
		
	};*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule__check_ins);
		
		final Calendar calender = Calendar.getInstance();
		DatePicker datePicker = null;
		TimePicker timePicker = null;
		calender.set(Calendar.MONTH, 11);
		calender.set(Calendar.YEAR, 2014);
		calender.set(Calendar.DAY_OF_MONTH, 15);
		
		calender.set(Calendar.HOUR_OF_DAY, 21);
		calender.set(Calendar.MINUTE, 54);
		calender.set(Calendar.SECOND, 0);
		calender.set(Calendar.AM_PM, Calendar.PM);
		
		Intent intent = new Intent(Schedule_CheckIns.this, CheckInReciever.class);
		pending_intent = PendingIntent.getBroadcast(Schedule_CheckIns.this, 0, intent, 0);
		
		final AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		
		
		final Button schedule = (Button) findViewById(R.id.button1);
		schedule.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v){
				//alarmManager.setRepeating(AlarmManager.RTC, calender.getTimeInMillis(),24*60*60*1000, pending_intent);
				Toast.makeText(getApplicationContext(), "ALARM DEMO", Toast.LENGTH_LONG).show();
				Intent timepicker_intent = new Intent(Schedule_CheckIns.this, Timepicker.class);
				startActivity(timepicker_intent);
			}
		});
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.schedule__check_ins, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}*/
}
