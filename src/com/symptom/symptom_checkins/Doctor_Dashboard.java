package com.symptom.symptom_checkins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
//import java.



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Doctor_Dashboard extends Activity {
	//Variables
	ListView alerts_patient;
	ListViewAdapter alert_adapter;
	EditText edit_alerts;
	String[] alerts;
	String[] cancers;
	String[] checkin_recent;
	
	ArrayList<Alerts> alerts_array = new ArrayList<Alerts>();
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor__dashboard);
		
		alerts = new String[] {"Paula Aliu","Anita LakeView","Ogata Nishimoto","Logan Sanders","Megan Hartford","Wendy Megana"};
		cancers = new String[] {"Throat Cancer", "Lung Cancer","Colon Cancer","Cervical Cancer","Brain Cancer","Skin Cancer"};
		
		alerts_patient = (ListView) findViewById(R.id.listView1);
		for (int i = 0 ; i < alerts.length; i++)
			{
				Alerts ale = new Alerts(alerts[i],cancers[i]);
				alerts_array.add(ale);
			}
		alert_adapter = new ListViewAdapter(this, alerts_array);
		alerts_patient.setAdapter(alert_adapter);
		alerts_patient.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		
		edit_alerts = (EditText) findViewById(R.id.editText1);
		edit_alerts.addTextChangedListener(new TextWatcher(){
			@Override
			public void afterTextChanged(Editable arg0){
				String text_alert = edit_alerts.getText().toString().toLowerCase(Locale.getDefault());
				alert_adapter.filter(text_alert);
				
				
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3){
			}
			
			@Override
			public void onTextChanged(CharSequence arg0,int arg1, int arg2, int arg3){
			}
		});
		
		alerts_patient.setMultiChoiceModeListener(new MultiChoiceModeListener(){

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				// TODO Auto-generated method stub
				mode.getMenuInflater().inflate(R.menu.doctor__dashboard,menu);
				return true;
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				// TODO Auto-generated method stub
				
				return false;
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
				// TODO Auto-generated method stub
				alert_adapter.removeSelection();
				
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode mode,
					int position, long id, boolean checked) {
				// TODO Auto-generated method stub
				final int checked_count = alerts_patient.getCheckedItemCount();
				mode.setTitle(checked_count + " Selected.");
				alert_adapter.toggleSelection(position);
				
			}
			
		});
			
		
		
		alerts_patient.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String product = ((TextView)view).getText().toString();
				String type_cancer = cancers[0];
				checkin_recent = new String[] {"Check-In 28/11/2014 12:09pm"};
				
				Intent i = new Intent(getApplicationContext(),Single_alertView.class);
				i.putExtra("Patient", product);
				i.putExtra("Cancer", type_cancer);
				i.putExtra("Recent CkeckIns", checkin_recent[0]);
				startActivity(i);
				
			}

			/*private int random(int length) {
				// TODO Auto-generated method stub
				return random((int) length);
			}*/
			
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId())
		{
		
		case R.id.action_settings:
			Toast.makeText(getApplicationContext(), "Settings Page", Toast.LENGTH_LONG).show();
			return true;
	
		
		case R.id.set_checkin:
			Intent checkin = new Intent(this.getApplicationContext(), Schedule_CheckIns.class);
			Toast.makeText(getApplicationContext(), "Schedulling  Page", Toast.LENGTH_LONG).show();
			startActivity(checkin);
			return true;
		
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
}


