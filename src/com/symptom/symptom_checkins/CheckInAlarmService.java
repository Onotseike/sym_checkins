package com.symptom.symptom_checkins;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class CheckInAlarmService extends Service {
	
	private NotificationManager checkin_manager;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
	}
	
	@SuppressWarnings("static_access")
	@Override
	public void onStart(Intent intent, int startId)
	{
		super.onStart(intent, startId);
		
		checkin_manager = (NotificationManager)this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);
		Intent intenta = new Intent(this.getApplicationContext(),Patient_Dashboard.class);
		
		Notification notif = new Notification(R.drawable.ic_launcher, "THIS IS A TEST CHECKIN...",System.currentTimeMillis());
		intenta.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		PendingIntent pending_notif_intent = PendingIntent.getActivity(this.getApplicationContext(), 0, intenta, PendingIntent.FLAG_UPDATE_CURRENT);
		notif.flags |= Notification.FLAG_AUTO_CANCEL;
		notif.setLatestEventInfo(this.getApplicationContext(), "CHECKIN DEMO", "TEST", pending_notif_intent);
		
		checkin_manager.notify(0,notif);
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}
	

}
