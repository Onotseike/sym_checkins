package com.symptom.symptom_checkins;

import java.util.Random;

import com.symptom.symptom_checkins.DoctorActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class PatientActivity extends Fragment {
	
	private EditText firstname ;
	private EditText med_id ;
	private Button login;
	
	private EditText middlename;
	private EditText lastname ;
	private EditText dob ;
	private EditText shared_key;
	
	/*@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);*/
	
		
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_patient, container,
				false);
		final Button signupat = (Button)rootView.findViewById(R.id.button2);
		signupat.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				if(checkSignup(firstname.getText(),lastname.getText(),dob.getText(),med_id.getText(),shared_key.getText()))
				{
					Toast.makeText(getActivity().getApplicationContext(), "Signing Up...", Toast.LENGTH_SHORT).show();
					//pat_dashboard = new Intent();
					final Intent pat_dashboard = new Intent(getActivity().getApplicationContext(),Patient_Dashboard.class); 
							//new Intent(PatientActivity.this,Patient_Dashboard.class);
					startActivity(pat_dashboard);
				}
				
				else
				{
					Toast.makeText(getActivity(), "Try Again...", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		final Button login_pat = (Button) rootView.findViewById(R.id.button1);
		login_pat.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				if(checkLogin(firstname.getText(), lastname.getText(),shared_key.getText())){
					Toast.makeText(getActivity().getApplicationContext(), "Login Successfull.....", Toast.LENGTH_SHORT).show();
					final Intent pat_dashboard = new Intent(getActivity().getApplicationContext(),Patient_Dashboard.class); 
					//new Intent(PatientActivity.this,Patient_Dashboard.class);
			startActivity(pat_dashboard);
				}
				else
				{
					firstname.setText("Try Again");
					shared_key.setText("Try again");
					Toast.makeText(getActivity().getApplicationContext(), "Login Failed.....", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		return rootView;
	}
	
	//@SuppressLint("CutPasteId")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		firstname = (EditText)getActivity().findViewById(R.id.editText1);
		lastname = (EditText)getActivity().findViewById(R.id.editText2);
		middlename = (EditText)getActivity().findViewById(R.id.editText3);
		dob = (EditText)getActivity().findViewById(R.id.editText4);
		shared_key = (EditText)getActivity().findViewById(R.id.editText5);
		med_id = (EditText)getActivity().findViewById(R.id.editText6); 
		
	}
	
	private boolean checkLogin(Editable username,Editable lastname,Editable userShared_Key){
		
		
		return new Random().nextBoolean();
		
	}
	
	private boolean checkSignup(Editable userfirst,Editable userlast,Editable userdob,Editable userMed_Num,Editable userShared_Key){
		return new Random().nextBoolean();
		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId())
		{
		
		case R.id.action_settings:
			Toast.makeText(getActivity().getApplicationContext(), "Settings Page", Toast.LENGTH_LONG).show();
			return true;
	
		
		case R.id.set_checkin:
			Intent checkin = new Intent(getActivity().getApplicationContext(), Schedule_CheckIns.class);
			Toast.makeText(getActivity().getApplicationContext(), "Schedulling  Page", Toast.LENGTH_LONG).show();
			startActivity(checkin);
			return true;
		
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
}
	